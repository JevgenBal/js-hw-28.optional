function displayList(arr, parent = $('body')) {
	let ul = $('<ul></ul>');
	parent.append(ul);

	arr.forEach((element) => {
		let li = $('<li></li>').text(element);
		ul.append(li);
	});
}

let arrCity = ["Hello", "world", "Kiev", ["Borispol","Bucha", "Irpin"], "Kharkiv", "Odessa", "Lviv"];
displayList(arrCity);

let arrNumbers = ["1", "2", "3", "sea", "user", 23];
displayList(arrNumbers);